import React from 'react';
import { Link } from 'react-router-dom';


export default function Navbar() {
  return (
    
    <div className='relative pt-6 lg:pt-8 flex items-center justify-between text-slate-700 font-semibold text-base leading-6 dark:text-slate-200'>
    <div className="text-slate-900 dark:text-white w-auto h-5 ml-7">Mon logo</div>
    <nav className="flex items-center justify-between">
      <div className="mx-36">
        <Link to="/" className="mx-10 no-underline hover: no underline hover:text-gray-800 dark:hover:text-gray-200">Accueil</Link>
        <Link to="about" className="mx-10 no-underline hover:no underline hover:text-gray-800 dark:hover:text-gray-200">A propos</Link>
        <Link to="contact" className="mx-10 no-underline hover: no underline hover:text-gray-800 dark:hover:text-gray-200">Contact</Link>
      </div>
    </nav>
  </div>
  );
}