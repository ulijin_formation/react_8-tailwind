import React, { useContext } from 'react';
import { ThemeContext } from '../../Context/ThemeContext';

export default function BtnToggle() {
  const { toggleTheme, theme } = useContext(ThemeContext);
  
  return (
    <button 
      onClick={toggleTheme}
      className="bg-gray-200 dark:bg-gray-800 text-gray-800 dark:text-gray-200 rounded-full p-2 h-8 w-16 flex items-center justify-center absolute top-3 right-0 mt-3 mr-4"
    >
      <span className="text-xs">{theme ? "LIGHT" : "DARK"}</span>
      <span className={`w-4 h-4 rounded-full bg-white shadow-md transform duration-300 ${theme ? 'translate-x-full' : ''}`}></span>
    </button>
  );
}