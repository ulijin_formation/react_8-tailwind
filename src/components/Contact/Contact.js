import React,{useContext}from 'react'
import BtnToggle from '../BtnToggle/BtnToggle';
import { ThemeContext } from '../../Context/ThemeContext';
export default function Contact() {
  const {theme}= useContext(ThemeContext)
  console.log(theme);
  return (
    <div className='container mx-auto px-4 py-8'>
    <BtnToggle />
    <h1 className='text-center text-4xl font-bold py-8'>Nous contacter</h1>
    <div className='content'>
        <p className='txt'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, numquam!</p>        
    </div>
    </div>
   
  )
}
