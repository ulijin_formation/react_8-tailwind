import React, { useContext } from 'react'
import { Link, Outlet } from 'react-router-dom'
import BtnToggle from '../BtnToggle/BtnToggle';
import { ThemeContext } from '../../Context/ThemeContext';

export default function About() {
  const { theme } = useContext(ThemeContext)
  console.log(theme);

  return (

    <div className='container mx-auto px-4 py-8'>
      <BtnToggle />
      <h1 className='text-center text-4xl font-bold py-8'>A propos</h1>
      <p className='txt-about'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam quibusdam ullam ratione eaque quia distinctio repellendus placeat accusamus dolore quam!</p>
      <nav className='sub-nav my-8'>
        <Link to="/about/sketch/" className="mx-10 no-underline hover: no underline hover:text-gray-800 dark:hover:text-gray-200">Maquettes</Link>
        <Link to="/about/output" className="mx-10 no-underline hover: no underline hover:text-gray-800 dark:hover:text-gray-200">Productions</Link>
      </nav>

      <Outlet />
    </div>
  )
}
