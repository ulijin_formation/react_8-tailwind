import React,{useContext}from 'react'
import BtnToggle from '../BtnToggle/BtnToggle';
import { ThemeContext } from '../../Context/ThemeContext';


export default function Home() {
  const {theme}= useContext(ThemeContext)
  console.log(theme);
  return (
    <div className='container mx-auto px-4 py-8'>
  <div className='flex justify-end'>
    <BtnToggle />
  </div>
  <h1 className='text-center text-4xl font-bold py-8'>Accueil</h1>
  <p className='text-center text-lg'>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at nisl odio. Aenean tincidunt, 
    elit eu blandit laoreet, nulla dolor lobortis lacus, eu imperdiet quam augue vel eros. 
    Morbi rhoncus congue sapien, eu malesuada nulla laoreet nec. Sed et justo a velit cursus 
    eleifend a in ipsum. Integer tristique sagittis enim, sit amet dignissim felis feugiat ac. 
    Donec a nulla in mi ullamcorper mollis sit amet eget nisl.
  </p>
</div>
  )
}
