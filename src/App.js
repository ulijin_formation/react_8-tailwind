
import './App.css';
import {Routes, Route} from "react-router-dom"
import Home from './components/Home/Home';
import Profile from './components/Profile/Profile';
import NotFound from './components/NotFound/NotFound';
import Navbar from './components/Navbar/Navbar';
import About from './components/About/About';
import Contact from './components/Contact/Contact';
import Sketch from './components/About/Sketch/Sketch';
import Output from './components/About/Output/Output';
import ThemeContextProvider from './Context/ThemeContext';


function App() {
  return (
    <div className="App">
      <ThemeContextProvider>
      <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="about" element={<About />}>
            <Route path="/about/sketch" element={<Sketch />} />
            <Route path="/about/output" element={<Output />} />
          </Route>
          <Route path="contact" element={<Contact />} />
          <Route path="/profil/:id" element={<Profile />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </ThemeContextProvider>
    </div>
  );
}

export default App;
